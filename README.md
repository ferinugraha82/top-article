# Top Article

Top Article adalah aplikasi yang membantu pengguna menemukan artikel-artikel terpopuler dan terbaru dari berbagai sumber.

## Fitur Utama
- Temukan artikel terbaru dan terpopuler dari berbagai sumber
- Baca artikel dengan tampilan yang bersih dan nyaman

## Instalasi
1. Clone repository ini
2. Buka terminal dan masuk ke direktori proyek
3. Jalankan perintah `flutter pub get`
4. Jalankan aplikasi dengan perintah `flutter run`

## Kompilasi Build APK
1. Buka terminal dan masuk ke direktori proyek
2. Jalankan perintah `flutter build apk`
3. Hasil build APK akan terletak pada direktori `/build/app/outputs/apk/release/`

## Unit Testing
1. Buka terminal dan masuk ke direktori proyek
2. Jalankan perintah `flutter test test/fetch_api_test.dart`
3. Hasil unit testing akan ditampilkan pada terminal

## Penggunaan
1. Buka aplikasi TOP Article
2. Temukan artikel terbaru dan terpopuler dari berbagai sumber
3. Pilih Article yang ingin dibaca
4. Baca Article yang diinginkan

## Referensi
- API NewsAPI (https://newsapi.org)
- Library Flutter (https://flutter.dev)