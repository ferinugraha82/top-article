import 'package:test/test.dart';
import 'package:top_article/utils/api_provider.dart';

void main() {
  test("Fetch API", () async {
    bool done = false;
    var fetch =
        (await ApiProvider().fetchArticles('category=sports', 'country=id'))
            .articles;
    if (fetch != null) {
      done = true;
    }
    expect(done, true);
  });
}
