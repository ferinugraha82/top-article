import 'dart:convert';
import '../models/articles.dart';
import 'package:http/http.dart' as http;

class ApiProvider {
  final String _baseUrl = 'https://newsapi.org/v2/top-headlines?';
  final String _apiKey = 'apiKey=a224bef6932340d7a2879c40e1848817';

  Future<Articles> fetchArticles(String category, String country) async {
    category = 'category=$category';
    country = 'country=$country';
    final response =
        await http.get(Uri.parse('$_baseUrl$category&$country&$_apiKey'));

    if (response.statusCode == 200) {
      return Articles.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load articles ${response.statusCode}');
    }
  }
}
