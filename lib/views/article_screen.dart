import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:skeletons/skeletons.dart';
import 'package:top_article/views/article_detail_screen.dart';
import '../models/article.dart';
import '../presenters/article_presenter.dart';

class ArticleScreen extends StatefulWidget {
  const ArticleScreen({Key? key}) : super(key: key);

  @override
  State<ArticleScreen> createState() => _ArticleScreenState();
}

class _ArticleScreenState extends State<ArticleScreen> {
  late ArticlePresenter _presenter;
  bool _isLoading = true;
  String searchCategory = '';

  @override
  void initState() {
    super.initState();
    _presenter = ArticlePresenter();
    _presenter.loadArticles(searchCategory, 'id').then((_) {
      setState(() {
        _isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: const <Widget>[
            Text(
              "TOP",
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: Color(0xFFF64E4E),
              ),
            ),
            Text(
              "Article",
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: Color(0xFF000000),
              ),
            ),
          ],
        ),
        backgroundColor: const Color(0xFFFFFFFF),
      ),
      body: SafeArea(
        child: Container(
            margin: const EdgeInsets.all(16.0),
            height: height,
            color: const Color(0xFFFFFFFF),
            child: Column(
              children: [
                Container(
                  width: width,
                  height: 40,
                  margin: const EdgeInsets.only(bottom: 10.0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: const Color(0xFFE0E0E0),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: TextField(
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 14.0,
                      fontWeight: FontWeight.w400,
                    ),
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                      prefixIcon: Icon(Icons.search, color: Color(0xFF929292)),
                      hintText: "Search category: sports, health, and science",
                      hintStyle: TextStyle(color: Color(0xFF929292)),
                      border: InputBorder.none,
                    ),
                    onChanged: (value) {
                      setState(() {
                        _isLoading = true;
                        searchCategory = value;
                        _presenter.loadArticles(searchCategory, 'id').then((_) {
                          _isLoading = false;
                        });
                      });
                    },
                  ),
                ),
                Expanded(child: _buildBody()),
              ],
            )),
      ),
    );
  }

  Widget _buildBody() {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    if (_isLoading) {
      return StaggeredGridView.countBuilder(
        staggeredTileBuilder: (index) => index == 0
            ? const StaggeredTile.count(2, 1)
            : const StaggeredTile.count(1, 1),
        crossAxisCount: 2,
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 10.0,
        itemCount: 10,
        itemBuilder: (BuildContext context, int index) {
          return SkeletonAvatar(
            style: SkeletonAvatarStyle(
              width: width,
              minHeight: height / 4,
              maxHeight: height / 3,
            ),
          );
        },
      );
    } else {
      return StaggeredGridView.countBuilder(
        staggeredTileBuilder: (index) => index == 0
            ? const StaggeredTile.count(2, 1)
            : const StaggeredTile.count(1, 1),
        crossAxisCount: 2,
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 10.0,
        itemCount: _presenter.articles.articles.length,
        itemBuilder: (BuildContext context, int index) {
          Article article = _presenter.articles.articles[index];

          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        ArticleDetailScreen(article: article)),
              );
            },
            child:
                _layoutArticle(article.title, article.author, article.urlImage),
          );
        },
      );
    }
  }

  Widget _layoutArticle(String title, String author, String img) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        Container(
          width: width,
          height: height * 0.3,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image:
                  DecorationImage(image: NetworkImage(img), fit: BoxFit.cover)),
        ),
        Container(
          width: width,
          height: height * 0.3,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            gradient: LinearGradient(
                colors: [
                  const Color(0xFF242424).withOpacity(0.2),
                  const Color(0xFF242424).withOpacity(0.2),
                ],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(1.0, 0.0),
                stops: const [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFFFFFFFF),
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
                child: Text(
                  "By $author",
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                    color: Color(0xFFFAFAFA),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
