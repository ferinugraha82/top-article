import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import '../models/articles.dart';
import '../utils/api_provider.dart';

class ArticlePresenter extends ChangeNotifier {
  late Articles _articles;

  Articles get articles => _articles;

  Future<void> loadArticles(String category, String country) async {
    ApiProvider apiProvider = ApiProvider();
    _articles = await apiProvider.fetchArticles(category, country);
    notifyListeners();
  }

  String reformatDate(String date) {
    final dateTime = DateTime.parse(date);
    final formatter = DateFormat('MMMM dd, yyyy');
    return formatter.format(dateTime);
  }
}
