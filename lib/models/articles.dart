import 'article.dart';

class Articles {
  final List<Article> articles;

  Articles({required this.articles});

  factory Articles.fromJson(Map<dynamic, dynamic> json) {
    List<Article> articles = (json['articles'] as List)
        .map((articleJson) => Article.fromJson(articleJson))
        .toList();
    return Articles(
      articles: articles,
    );
  }
}
