class Article {
  final String title;
  final String publishedAt;
  final String content;
  final String author;
  final String urlImage;
  final String url;

  Article(
      {required this.title,
      required this.publishedAt,
      required this.content,
      required this.author,
      required this.urlImage,
      required this.url});

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article(
        title: json['title'] ?? '',
        publishedAt: json['publishedAt'] ?? '',
        content: json['content'] ?? '',
        author: json['author'] ?? '',
        urlImage: json['urlToImage'] ?? '',
        url: json['url'] ?? '');
  }
}
